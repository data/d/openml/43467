# OpenML dataset: National-Hockey-League-Interviews

https://www.openml.org/d/43467

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was scraped from http://www.asapsports.com/, using the code in this repository. I designed the webscraping code to account for most of the variance in the website's formatting, but some webpages with formatting that differed significantly were ignored. While manually inspecting random rows of the dataset I did not notice any glaring errors in the transcripts, but I cannot guarantee that there aren't any. 
Content
RowId: A unique row identifier
team1 and team2: The two teams in the Stanley Cup Final. Whether a team is team1 or team2 has no meaning: it's determined by the order of their listing on the website.
date: The date of the interview
name: The person being interviewed
job: Takes values "player", "coach", and "other". If they are a player or coach at the time of the interview they are assigned accordingly. Otherwise they are assigned "other". Most of the people in the "other" category are general managers, league officials, and commentators. Some of these values were assigned automatically based on their title in a transcript (e.g. "Coach Mike Babcock"), and others were assigned manually. A possible source of error is the fact that I did not manually inspect names that appeared only once. 
text: The interview transcript. Interviewer questions were not collected, so all of the speech comes from the interviewee. Responses to questions are separated by periods. These periods will be the only punctuation in the text. Note that a likely source of error in this column is a failure to ignore an interviewer's questions. 
Related Links
In other work I used this dataset to train an RNN-based Facebook Messenger chatbot to respond to messages as a hockey player might. More precisely, if you send the bot the beginning of an interview response, it will respond with a 5-sentence continuation of that response. For example, it could receive "Well you know" and respond with "Well you know we played hard out there and". Follow this link to interact with the chatbot and this link to read the Medium article where I explain how I created the bot.
Acknowledgements
The idea to scrape interviews from ASAPSports came from this article by Mathieu Bray.
Inspiration

How do speech patterns of NHL coaches and players differ? Are coaches more positive than players? More team oriented?
How have hockey interview responses changed over the years?
Can we create a bot that talks like an NHL player? (See Related Links)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43467) of an [OpenML dataset](https://www.openml.org/d/43467). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43467/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43467/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43467/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

